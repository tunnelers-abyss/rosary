local small_dots = {
	{x=11, y=17},
	{x=17, y=21},
	{x=24, y=22},
	{x=27, y=16},
	{x=26, y=9},
	{x=21, y=4},
	{x=15, y=2},
	{x=9, y=2},
	{x=4, y=7},
	{x=3, y=13},
}

local big_dots = {
	{x=18, y=12}, {x=22, y=14}, {x=22, y=18}, {x=23, y=22}, {x=25, y=26}, {x=26, y=30}, {x=30, y=31}, {x=34, y=29}, {x=35, y=25}, {x=35, y=20}, {x=38, y=13},
	{x=45, y=14}, {x=47, y=18}, {x=47, y=22}, {x=46, y=26}, {x=47, y=30}, {x=48, y=34}, {x=47, y=38}, {x=43, y=40}, {x=39, y=41}, {x=35, y=40}, {x=31, y=45},
	{x=33, y=51}, {x=34, y=55}, {x=38, y=56}, {x=42, y=57}, {x=46, y=58}, {x=50, y=59}, {x=54, y=57}, {x=55, y=53}, {x=56, y=49}, {x=56, y=45}, {x=55, y=39},
	{x=55, y=32}, {x=56, y=28}, {x=57, y=24}, {x=57, y=20}, {x=57, y=16}, {x=57, y=12}, {x=56, y=8}, {x=55, y=4}, {x=51, y=3}, {x=47, y=2}, {x=40, y=2},
	{x=33, y=2}, {x=29, y=1}, {x=25, y=2}, {x=21, y=3}, {x=17, y=2}, {x=13, y=1}, {x=9, y=1}, {x=5, y=1}, {x=4, y=5}, {x=6, y=9}, {x=11, y=13},
	{x=11, y=19}, {x=11, y=23}, {x=11, y=27}, {x=11, y=33},
}
-- items

minetest.register_craftitem("rosary:small_0", {
	description = "Small rosary",
	inventory_image = "rosary_small.png",
	wield_scale = {x=1, y=1, z=0.5},
	groups = {rosary=1, rosary_small=1},
	stack_max = 1,
	on_use = function(itemstack, user, pointed_thing)
		itemstack:set_name("rosary:small_10")
		return itemstack
	end,
})

minetest.register_craftitem("rosary:big_0", {
	description = "Large rosary",
	inventory_image = "rosary_big.png",
	wield_image = "[combine:96x96:0,0=rosary_big.png:",
	wield_scale = {x=1.5, y=1.5, z=0.5},
	groups = {rosary=1, rosary_big=1},
	stack_max = 1,
	on_use = function(itemstack, user, pointed_thing)
		itemstack:set_name("rosary:big_59")
		return itemstack
	end,
})

for i, p in ipairs(small_dots) do
	minetest.register_craftitem("rosary:small_"..i, {
		description = "Small rosary",
		inventory_image = "[combine:32x32:0,0=rosary_small.png:"..p.x..","..p.y.."=rosary_dot.png",
		wield_scale = {x=1, y=1, z=0.5},
		groups = {rosary=1, rosary_small=1, not_in_creative_inventory=1},
		stack_max = 1,
		on_use = function(itemstack, user, pointed_thing)
			itemstack:set_name("rosary:small_"..(i-1))
			return itemstack
		end,
	})
end

for i, p in ipairs(big_dots) do
	minetest.register_craftitem("rosary:big_"..i, {
		description = "Large rosary",
		inventory_image = "[combine:64x64:0,0=rosary_big.png:"..p.x..","..p.y.."=rosary_dot.png",
		wield_image = "[combine:96x96:0,0=rosary_big.png:"..p.x..","..p.y.."=rosary_dot.png",
		wield_scale = {x=1.5, y=1.5, z=0.5},
		groups = {rosary=1, rosary_big=1, not_in_creative_inventory=1},
		stack_max = 1,
		on_use = function(itemstack, user, pointed_thing)
			itemstack:set_name("rosary:big_"..(i-1))
			return itemstack
		end,
	})
end

-- crafts

minetest.register_craft({
	type = "shapeless",
	output = "rosary:big_0",
	recipe = {
		"group:rosary_small",
		"group:rosary_small",
		"group:rosary_small",
		"group:rosary_small",
		"group:rosary_small",
	}
})

minetest.register_craft({
	output = "rosary:small_0",
	recipe = {
		{"default:steel_ingot","farming:string","default:steel_ingot"},
		{"farming:string","","farming:string"},
		{"default:steel_ingot","farming:string","default:steel_ingot"},
	}
})

